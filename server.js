const express = require('express')
const app = express()
const port = 3000
app.use(express.json())

app.get('/start', (req, res) => {
    res.send('Вторая стартовая страница');
    console.log("get('/start') Отправили вторую стартовую страницу");
})
app.post('/p', (req, res) => {
    res.send(req.body)
    console.log("post('/p') Вернули параметры запроса")
})

app.put('/myput', (req, res) => {
    res.send(req.body)
    console.log("put('/myput') Вернули параметры запроса")
})

app.delete('/del', (req, res) => {
    res.send('Удаление успешно')
    console.log("delete('/del') Вернули удаление успешно")
})

app.post('/pot/:param', (req, res) => {
    res.send(req.params)
    console.log("post('/posr/:param') Вернули ваши параметры:", req.params)
})

app.use(express.static('public'))

app.listen(port, () => {
    console.log(`Example app listening on port http://localhost:${port}`)
})
